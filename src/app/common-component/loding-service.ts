import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
@Injectable({ providedIn: 'root' })
export class LoadingService {

    private _loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    get loadingSubject() {
        return this._loadingSubject
    }
    loadingSubjectObservable: Observable<boolean> = this._loadingSubject.asObservable();
    constructor() {

    }

    public getLoadData(): Observable<boolean> {
        return this.loadingSubjectObservable;
    }
}