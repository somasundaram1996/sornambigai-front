export class FileDownloadUtil {
    public static downloadFile(base64EncodedByteArry: any, fileName) {
        var binary_string =  window.atob(base64EncodedByteArry);
        var len = binary_string.length;
        var bytes = new Uint8Array( len );
        for (var i = 0; i < len; i++)        {
            bytes[i] = binary_string.charCodeAt(i);
        }
        var blob, url;
        blob = new Blob([bytes.buffer as BlobPart], {
            type: 'application/pdf'
        });
        url = window.URL.createObjectURL(blob);
        var anchorElement = document.createElement('a');
        anchorElement.href = url;
        anchorElement.download = fileName;
        document.body.appendChild(anchorElement);
        anchorElement.click();
        anchorElement.remove();
        setTimeout(function () {
            return window.URL.revokeObjectURL(url);
        }, 1000);
    }
}