import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'customCurrencyPipe'})
export class CustomCurrencyPipe implements PipeTransform {
  transform(value: any): string {
    return '₹ ' + value;
  }
}