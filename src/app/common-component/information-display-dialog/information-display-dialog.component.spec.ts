import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationDisplayDialogComponent } from './information-display-dialog.component';

describe('InformationDisplayDialogComponent', () => {
  let component: InformationDisplayDialogComponent;
  let fixture: ComponentFixture<InformationDisplayDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationDisplayDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationDisplayDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
