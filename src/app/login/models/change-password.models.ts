export interface ChangePasswordResponseModel {
    result: boolean;
    message: string;
}