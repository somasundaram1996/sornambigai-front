import { LoadingService } from './common-component/loding-service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map, finalize } from 'rxjs/operators'
import { Router } from '@angular/router';
import { ToasterService } from './toaster.service';

@Injectable({
  providedIn: 'root'
})
export class CommonHttpInteceptorService implements HttpInterceptor {

  constructor(private _router: Router, private _toaster: ToasterService, private loadingService: LoadingService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (sessionStorage.getItem('jwtToken')) {
      req = req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + sessionStorage.getItem('jwtToken')
        }
      })
    }
    this.loadingService.loadingSubject.next(true);
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          this._toaster.error('ERROR', 'Client Error Sorry for the inconvenience');
        } else {
          console.log(req.headers);
          if (!req.headers.has('fromLogin')) {
            if (error.status === 401) {
              this._toaster.info('Info', 'Session Timeout!!!!');
              this._router.navigate(['/login']);
            } else if (error.status === 400 || error.status === 404) {
              this._toaster.error('ERROR', 'Invalid Request!');
            } else if (error.status === 503 || error.status === 504) {
              this._toaster.error('ERROR', 'Server Unavailale. Try After Sometime.');
            } else {
              this._toaster.error('ERROR', 'Server Error Sorry for the inconvenience');
            }
          } else {
            return throwError(error);
          }
        }
        return throwError(errorMessage);
      }), finalize(() => {
        this.loadingService.loadingSubject.next(false);
      }));
  }
}
