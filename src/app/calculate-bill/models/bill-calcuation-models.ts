export interface BillCalcuationRequestModel {
    billHeader: BillHeaderModel;
    itemDetails: ItemDetailModel;
}

export interface BillCalculationResponseModel {
    totalAmount:number;
    itemDetails:ItemDetailModel;
}

export interface BillHeaderModel {
    billId: string;
    billStatus: 'OPEN' | 'DRAFT' | 'CLOSED';
    customerName: string;
    cashierName: string;
    billRegisteredDate?: Date;
    items: Array<ItemDetailModel>;
    totalBillAmount: number;
}
export interface ItemDetailModel {
    itemId: number;
    itemName: string;
    itemCategoryId: string;
    pricePerGramOnPurchase: number;
    gramsPurchased: number;
    wastage: number;
    makingCharge: number;
    quantity: number;
    itemAmount: number;
  }