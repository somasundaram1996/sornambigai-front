export interface FileResponseModel {
    fileData: string;
    fileName: string;
    result: boolean;
    failMessage: string;
    billId: string;
}