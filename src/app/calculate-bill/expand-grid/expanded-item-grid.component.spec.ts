import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandedItemGridComponent } from './expanded-item-grid.component';

describe('ExpandedItemGridComponent', () => {
  let component: ExpandedItemGridComponent;
  let fixture: ComponentFixture<ExpandedItemGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpandedItemGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandedItemGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
