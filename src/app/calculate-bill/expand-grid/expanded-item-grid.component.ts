import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomDataSource } from './../grid-datasource/grid-datasource';
import { Component, OnInit, Inject } from '@angular/core';
export interface ExpandGridData {
  dataSource: CustomDataSource,
  itemCategoryTable: any[];
  totalAmount: number
}
@Component({
  selector: 'app-expanded-item-grid',
  templateUrl: './expanded-item-grid.component.html',
  styleUrls: ['./expanded-item-grid.component.css']
})
export class ExpandedItemGridComponent {
  displayedColumns: string[] = ['itemId', 'item', 'itemCategory', 'costPerGram', 'makingCharge',
    'gramsPurchased', 'wastage', 'quantity', 'itemAmount'];
  constructor(
    public dialogRef: MatDialogRef<ExpandedItemGridComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExpandGridData) {
  }


  getTotalCost() {
    let total = 0
    this.data.dataSource.returnData().subscribe(result => {
      total = result.map(t => t.itemAmount).reduce((acc, value) => acc + value, 0);
    });
    return total;
  }

  getItemCategoryName(itemCategoryId) {
    if (this.data.itemCategoryTable && itemCategoryId) {
      return this.data.itemCategoryTable.find(itemCategory => itemCategoryId === itemCategory.itemCategoryId).itemCategoryName;
    } else {
      return "";
    }
  }
}
