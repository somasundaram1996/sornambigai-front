import { ItemDetailModel } from './../models/bill-calcuation-models';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';

export class CustomDataSource extends DataSource<ItemDetailModel> {
    constructor(private myArray: BehaviorSubject<ItemDetailModel[]>){
      super();
    }
    connect(collection: CollectionViewer): Observable<ItemDetailModel[]> {
      return this.myArray.asObservable();
    }
    returnData() {
      return this.myArray.asObservable();
    }
    disconnect(collection: CollectionViewer): void {
    }
  }