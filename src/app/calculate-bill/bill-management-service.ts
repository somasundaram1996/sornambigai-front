import { Base } from './../resources/common/base-url';
import { Observable } from 'rxjs';
import { ItemModel } from './../add-jewell/add-jewell.component';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BillCalculationResponseModel, BillHeaderModel } from './models/bill-calcuation-models';
import { FileResponseModel } from './models/file-response-model';
export interface PriceModel {
    itemCategoryId: string;
    pricePerGram: number;
}
@Injectable()
export class BillManagementService {
    constructor(private _http: HttpClient) { }
    base: Base = new Base();
    loadItems(param): Observable<ItemModel[]> {
        return this._http.post<ItemModel[]>(this.base.getBaseUrl() + "/auth/getItems", param);
    }

    getPriceList(): import("rxjs").ObservableInput<PriceModel[]> {
        return this._http.get<PriceModel[]>(this.base.getBaseUrl() + '/auth/getPrice');
    }

    getItemCategories(): Observable<any> {
        return this._http.post(this.base.getBaseUrl() + "/auth/getItemCategories", {});
    }

    addItemToBilAfterCalculation(params): Observable<BillCalculationResponseModel> {
        return this._http.post<BillCalculationResponseModel>(this.base.getBaseUrl() + '/auth/calculateForSingleItem', params);
    }

    getBillByBillId(billId: string):Observable<BillHeaderModel> {
        return this._http.get<BillHeaderModel>(this.base.getBaseUrl()+'/auth/getBillById?billId='+billId);
    }
    saveBill(params: BillHeaderModel): Observable<BillHeaderModel> {
        return this._http.post<BillHeaderModel>(this.base.getBaseUrl()+'/auth/insertBill',params);
    }

    closeAndExportBill(params: BillHeaderModel):Observable<FileResponseModel> {
        return this._http.post<FileResponseModel>(this.base.getBaseUrl()+'/auth/closeAndPrintBill',params);
    }

    deleteByBillId(billId: string): Observable<boolean> {
        return this._http.post<boolean>(this.base.getBaseUrl()+'/auth/deleteByBillId?billId='+billId,{});
    }

    printBill(billId: string):Observable<FileResponseModel> {
        return this._http.get<FileResponseModel>(this.base.getBaseUrl()+'/auth/printBill?billId='+ billId);
    }
}