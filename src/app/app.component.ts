import { LoadingService } from './common-component/loding-service';
import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  isLoading:boolean= false;
  constructor(private loadingService:LoadingService, private changeDetector: ChangeDetectorRef){
  }

  ngAfterViewInit() {
    this.loadingService.loadingSubject.subscribe(value =>{
      this.isLoading =value;
      this.changeDetector.detectChanges();
  });
  }
}
