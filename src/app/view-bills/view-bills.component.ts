import { BillHeaderModel } from './../calculate-bill/models/bill-calcuation-models';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from './../toaster.service';
import { ViewBillsService } from './view-bills.service';
import { map, debounceTime, distinctUntilChanged, switchMap, delay } from 'rxjs/operators';
import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Observable, of, forkJoin, fromEvent } from 'rxjs';
@Component({
  selector: 'app-view-bills',
  templateUrl: './view-bills.component.html',
  styleUrls: ['./view-bills.component.css'],
  providers: [ViewBillsService]
})
export class ViewBillsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['billId', 'billRegisterdDate', 'customerName', 'cashierName', 'billAmount', 'billStatus'];
  @ViewChild('filter') filter: ElementRef;
  @ViewChild('gridContainer') gridContainer: ElementRef
  dataSource = new MatTableDataSource([]);
  keyword: string='';
  constructor(private _service: ViewBillsService, private toaster: ToasterService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    forkJoin(this._service.searchBills('', 0)).subscribe(result => {
      this.dataSource.data = result[0];
    })
  }

  openDetail(row) {
    this.router.navigate(['../calculateBill'], {
      relativeTo: this.route,
      queryParams: { billId: row.billId }
    })
  }

  ngAfterViewInit() {
    fromEvent(this.filter.nativeElement, "keyup").pipe(debounceTime(1000),
      map((event: Event) => (event.target as HTMLInputElement).value),
      distinctUntilChanged(),
      switchMap(value => this.searchValue(value))).subscribe(result => {
        this.dataSource.data = result;
      });
    fromEvent(this.gridContainer.nativeElement, 'scroll').pipe(
      map((event: Event) => (event.target as HTMLDivElement).scrollHeight),
      switchMap(scrollHeight => {
        if (this.gridContainer.nativeElement.scrollTop === (this.gridContainer.nativeElement.scrollHeight - this.gridContainer.nativeElement.offsetHeight)) {
          return this.onScroll();
        }
        return [];
      })).subscribe(fetchedresult => {
        this.dataSource.data = this.dataSource.data.concat(fetchedresult);
      });
  }

  searchValue(keyword: string): Observable<BillHeaderModel[]> {
    this.keyword = keyword;
    return this._service.searchBills(keyword, 0);
  }

  onScroll() {
    return this._service.searchBills(this.keyword, this.dataSource.data.length);
  }

}
