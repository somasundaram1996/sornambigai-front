import { Injectable } from '@angular/core';
import { Base } from './../resources/common/base-url';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BillHeaderModel } from './../calculate-bill/models/bill-calcuation-models';
@Injectable()
export class ViewBillsService {
    _base: Base = new Base();
    constructor(private _http: HttpClient) { }

    searchBills(keyWord: string, offset: number): Observable<BillHeaderModel[]> {
        return this._http.get<BillHeaderModel[]>(this._base.getBaseUrl() + '/auth/searchBills?keyword=' + keyWord + '&offset=' + offset);
    }
}